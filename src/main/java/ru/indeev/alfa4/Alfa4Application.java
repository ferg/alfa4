package ru.indeev.alfa4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Alfa4Application {

	public static void main(String[] args) {
		SpringApplication.run(Alfa4Application.class, args);
	}

}
